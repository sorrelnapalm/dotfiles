#!/bin/bash

echo 'terminating gammastep...'
pkill -15 gammastep
echo 'starting wait...'
nohup sleep 7 >/dev/null 2>&1
wait
echo 'starting gammastep...'
gammastep -t 6500K:3000K -b 1.0:0.85 -l 34:-118 &
